package com.example.currentweatherdatabinding

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import com.example.currentweatherdatabinding.databinding.ActivityMainBinding
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.InputStream
import java.net.URL
import java.util.*

fun SnapHelper.getSnapPosition(recyclerView: RecyclerView): Int {
    val layoutManager = recyclerView.layoutManager ?: return RecyclerView.NO_POSITION
    val snapView = findSnapView(layoutManager) ?: return RecyclerView.NO_POSITION
    return layoutManager.getPosition(snapView)
}

fun RecyclerView.attachSnapHelperWithListener(
    snapHelper: SnapHelper,
    behavior: SnapOnScrollListener.Behavior = SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL,
    onSnapPositionChangeListener: OnSnapPositionChangeListener?,
    cities: MutableList<String>,
    context: Context,
    binding: ActivityMainBinding
) {

    snapHelper.attachToRecyclerView(this)
    val snapOnScrollListener = SnapOnScrollListener(snapHelper, behavior, onSnapPositionChangeListener, context, cities, binding)
    addOnScrollListener(snapOnScrollListener)
}
interface OnSnapPositionChangeListener {

    fun onSnapPositionChange(position: Int)
}

class SnapOnScrollListener(
    private val snapHelper: SnapHelper,
    var behavior: Behavior = Behavior.NOTIFY_ON_SCROLL,
    var onSnapPositionChangeListener: OnSnapPositionChangeListener? = null,
    val context: Context,
    val cities: MutableList<String>,
    val binding: ActivityMainBinding
) : RecyclerView.OnScrollListener() {


//    val activeColor = ContextCompat.getColor(context, R.color.activeColor)
//    val nonActiveColor = ContextCompat.getColor(context, R.color.nonActiveColor)



    enum class Behavior {
        NOTIFY_ON_SCROLL,
        NOTIFY_ON_SCROLL_STATE_IDLE
    }

    private var snapPosition = RecyclerView.NO_POSITION

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (behavior == Behavior.NOTIFY_ON_SCROLL) {
            maybeNotifySnapPositionChange(recyclerView)
        }
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        if (behavior == Behavior.NOTIFY_ON_SCROLL_STATE_IDLE
            && newState == RecyclerView.SCROLL_STATE_IDLE) {
            maybeNotifySnapPositionChange(recyclerView)
        }
    }

    private fun maybeNotifySnapPositionChange(recyclerView: RecyclerView) {
        val snapPosition = snapHelper.getSnapPosition(recyclerView)
        val snapPositionChanged = this.snapPosition != snapPosition
        if (snapPositionChanged) {
            onSnapPositionChangeListener?.onSnapPositionChange(snapPosition)
            this.snapPosition = snapPosition
            val holder = recyclerView.findViewHolderForLayoutPosition(snapPosition) as CustomRecyclerAdapter.MyViewHolder
            holder.cityName.setBackgroundDrawable(context.resources.getDrawable(R.drawable.round_corners_active))
            val holderPrev = recyclerView.findViewHolderForLayoutPosition(snapPosition - 1) as CustomRecyclerAdapter.MyViewHolder
            holderPrev.cityName.setBackgroundDrawable(context.resources.getDrawable(R.drawable.round_corners))
            val holderNext = recyclerView.findViewHolderForLayoutPosition(snapPosition + 1) as CustomRecyclerAdapter.MyViewHolder
            holderNext.cityName.setBackgroundDrawable(context.resources.getDrawable(R.drawable.round_corners))

            GlobalScope.launch (Dispatchers.IO) {
                loadWeather(context, cities, snapPosition, binding)
            }


        }
    }

    private suspend fun loadWeather(context: Context, cities: MutableList<String>, snapPosition: Int, binding: ActivityMainBinding) {
        val city = cities[snapPosition % cities.size]
        Log.d("NIKITA", city)

        val API_KEY = context.resources.getString(R.string.api_key)
        val weatherURL = "https://api.openweathermap.org/data/2.5/weather?q=${java.net.URLEncoder.encode(city, "utf-8")}&appid=$API_KEY&units=metric";
        try {
            val stream = URL(weatherURL).getContent() as InputStream
            // JSON отдаётся одной строкой,
            val data = Scanner(stream).nextLine()
            Log.d("mytag", data)
            val weatherData: WeatherData = Gson().fromJson<WeatherData>(data, WeatherData::class.java)
            val weatherViewData = WeatherViewData(weatherData)
//            weatherViewData.dayLength = weatherData.sys?.sunset!! - weatherData.sys?.sunrise!!
//            weatherViewData.windDegrees = weatherData.wind?.deg!!
//            weatherViewData.weatherIcon = weatherData.weather[0].icon!!
            binding.weather = weatherViewData
        }
        catch (e: java.lang.Exception){
            Log.d("NIKITA",
                e.toString())
            binding.textView.text = context.getString(R.string.error)
        }
    }
}